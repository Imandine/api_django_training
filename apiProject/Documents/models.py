from django.db import models
from django.contrib.auth import get_user_model

User = get_user_model()

class Categories(models.Model):
    nom = models.CharField(max_length=100, unique=True)
    
    def __str__(self) -> str:
        return f"<Categorie {self.nom}>"

class Documents(models.Model):
    NIVEAUX = (
        ('AUTRE', 'Autre'),
        ('COLLEGE', 'College'),
        ('UNIVERSITAIRE', 'Universitaire'),
    )
    titre = models.CharField(max_length=100)
    description = models.TextField()
    examplaires = models.IntegerField()
    niveau = models.CharField(choices=NIVEAUX, default=NIVEAUX[0][0], max_length=20)
    fichier = models.FileField(upload_to ='Medias/', blank=True, null=True)
    categorie = models.ForeignKey(Categories, on_delete=models.SET_NULL, null=True)
    bibliophiles = models.ManyToManyField(User, related_name='documents', blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    
    def __str__(self) -> str:
        return f"<Documents {self.titre}>"
