from csv import list_dialects
from django.contrib import admin
from .models import Categories, Documents

admin.site.register(Categories)

@admin.register(Documents)
class DocumentsAdmin(admin.ModelAdmin):
    list_display = ['titre', 'description', 'niveau', 'examplaires', 'created_at']
    list_filter = ['created_at', 'niveau']