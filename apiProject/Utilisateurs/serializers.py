from  rest_framework import serializers
from .models import Users

class UserCreationSerializers(serializers.ModelSerializer):
    
    email = serializers.EmailField(max_length=80)
    username = serializers.CharField(max_length=30)
    phone = serializers.CharField(max_length=20)
    password = serializers.CharField(min_length=8)
    
    class Meta:
        model = Users
        fields = ['email', 'username', 'phone', 'password']
        
    def validate(self, attrs):
        username_exists = Users.objects.filter(username=attrs['username']).exists()
        if username_exists:
            raise serializers.ValidationError("Username existe")
        
        email_exists = Users.objects.filter(email=attrs['email']).exists()
        if email_exists:
            raise serializers.ValidationError("Email existe")
        
        return super().validate(attrs)
    
class UserLoginSerializers(serializers.ModelSerializer):
    
    email = serializers.EmailField(max_length=80)
    password = serializers.CharField(min_length=8)
    
    class Meta:
        model = Users
        fields = ['email', 'password']
        
    def validate(self, attrs):
        
        email_exists = Users.objects.filter(email=attrs['email']).exists()
        if not email_exists:
            raise serializers.ValidationError("Email n'existe pas")
        
        return super().validate(attrs)