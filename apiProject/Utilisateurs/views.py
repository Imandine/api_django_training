import email
from rest_framework import generics, status
from rest_framework.response import Response
from .serializers import UserCreationSerializers, UserLoginSerializers
from rest_framework.authtoken.models import Token
from rest_framework.permissions import AllowAny
from django.contrib.auth import authenticate
from .models import Users


class CustomAuthToken(generics.GenericAPIView):
    permission_classes = (AllowAny,)
    serializer_class= UserLoginSerializers
    def post(self, request):
        serializer = self.serializer_class(data=request.data,
                                           context={'request': request})
        if serializer.is_valid():
            email = serializer.validated_data['email']
            password = serializer.validated_data['password']
            user = authenticate(email=email, password=password)
            print(user)
            if user:
                token, created = Token.objects.get_or_create(user=user)
                return Response({
                    'token': token.key,
                    'user_id': user.pk,
                    'email': user.email
                })
            return Response({'errors': 'Email ou password n\'existe pas'}, status=status.HTTP_400_BAD_REQUEST)
        
        return Response(data=serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class UserCreateView(generics.GenericAPIView):
    
    serializer_class= UserCreationSerializers
    
    def post(self, request):
        data = request.data
        Serializer = self.serializer_class(data=data)
        
        if Serializer.is_valid():
            Serializer.save()
            return Response(data=Serializer.data, status=status.HTTP_201_CREATED)
        
        return Response(data=Serializer.errors, status=status.HTTP_400_BAD_REQUEST)
