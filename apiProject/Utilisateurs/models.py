import email
from select import select
from django.db import models
from django.contrib.auth.models import AbstractUser
from django.contrib.auth.base_user import BaseUserManager

class MyUserManager(BaseUserManager):
    
    def create_user(self, email, password, **extra_fields):
        if not email:
            raise ValueError("Email should be provided")
        
        email = self.normalize_email(email)
        new_user = self.model(email=email, **extra_fields)
        new_user.set_password(password)
        new_user.save()
        
        return new_user
    
    def create_superuser(self, email, password, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)
        extra_fields.setdefault('is_active', True)
        
        if not extra_fields.get('is_staff') or not extra_fields.get('is_superuser') or not extra_fields.get('is_active'):
            raise ValueError("SuperUser should have is_staff and is_superuser and is_active as True")
        
        return self.create_user(email, password, **extra_fields)
    
class Users(AbstractUser):
    email = models.EmailField(max_length=80, unique=True)
    username = models.CharField(max_length=30, unique=True)
    phone = models.CharField(max_length=20, blank=True, null=True)
    
    USERNAME_FIELD = 'email'
    
    REQUIRED_FIELDS = ['username']
    
    objects = MyUserManager()
    
    def __str__(self) -> str:
        return f"<User: {self.email} ({self.phone})>" 
    
