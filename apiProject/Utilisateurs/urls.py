from django.urls import path
from .views import UserCreateView, CustomAuthToken

urlpatterns = [
    path('', UserCreateView.as_view()),
    path('token', CustomAuthToken.as_view()),
]
